#!/bin/sh
rm -rf target/*
mkdir -p target
cd target && mvn archetype:generate -DinteractiveMode=false\
	-DarchetypeGroupId=com.netceler.ivtracer.archetypes\
	-DarchetypeArtifactId=cockpit-archetype\
	-DarchetypeVersion=1.2-SNAPSHOT\
	-Dcompany=netceler\
	-Dtown=veynes\
	-DsiteName="Netceler Veynes"\
	-DcockpitVersion=2.3.0.0\
	-DserviceAccountPassword=netc\
	-DartifactId=netceler-veynes-cockpit\
	-DgroupId=g -Dpackage=pom && cd -
