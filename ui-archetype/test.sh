#!/bin/sh
rm -rf target/*
mkdir -p target
cd target && mvn archetype:generate -DinteractiveMode=false\
	-DarchetypeGroupId=com.netceler.ivtracer.archetypes\
	-DarchetypeArtifactId=ui-archetype\
	-DarchetypeVersion=1.2-SNAPSHOT\
	-Dcompany=netceler\
	-Dtown=veynes\
	-DsiteName="Netceler Veynes"\
	-DivtracerUIVersion=2.3-0.0\
	-DserviceAccountPassword=netc\
	-DartifactId=netceler-veynes-ui\
	-DgroupId=g -Dpackage=pom && cd -
