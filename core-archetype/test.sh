#!/bin/sh
rm -rf target/*
mkdir -p target
cd target && mvn archetype:generate -DinteractiveMode=false\
	-DarchetypeGroupId=com.netceler.ivtracer.archetypes\
	-DarchetypeArtifactId=core-archetype\
	-DarchetypeVersion=1.2-SNAPSHOT\
	-Dcompany=netceler\
	-Dtown=veynes\
	-DsiteName="Netceler Veynes"\
	-DivtracerCoreVersion=2.3.0.0\
	-DflyboxVersion=1.3.4\
	-DserviceAccountPassword=netc\
	-DartifactId=netceler-veynes-server-install\
	-DgroupId=g -Dpackage=pom && cd -
