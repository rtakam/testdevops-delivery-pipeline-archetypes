#!/bin/sh
# $1 est le numero de version a builder

IVT_TEMPLATE_FOLDER=target/ivtracer-server-template

rm target/IVTracer-server-patch-$1.zip
rm -r target/IVTracer-server-patch-$1
mkdir target/IVTracer-server-patch-$1

mkdir -p target/IVTracer-server-patch-$1/engine/infocenter/
cp -r $IVT_TEMPLATE_FOLDER/intravision/ivtracer/engine/infocenter/deploy target/IVTracer-server-patch-$1/engine/infocenter/

mkdir -p target/IVTracer-server-patch-$1/java/jre/lib/ext
cp -r $IVT_TEMPLATE_FOLDER/intravision/ivtracer/java/jre/lib/ext/ivtracer-jasper-java-override* target/IVTracer-server-patch-$1/java/jre/lib/ext

cp applyIVTracerPatch.bat target/IVTracer-server-patch-$1/
sed -i 's/%VERSION%/'$1'/g' target/IVTracer-server-patch-$1/applyIVTracerPatch.bat

cd target
zip -r IVTracer-server-patch-$1.zip IVTracer-server-patch-$1