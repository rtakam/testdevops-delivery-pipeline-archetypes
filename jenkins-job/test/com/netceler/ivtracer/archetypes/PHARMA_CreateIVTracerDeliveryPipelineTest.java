package com.netceler.ivtracer.archetypes;

import org.junit.Assert;
import org.junit.Test;

import com.google.common.collect.ImmutableMap;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import groovy.lang.Script;

public class PHARMA_CreateIVTracerDeliveryPipelineTest extends AbstractIVTracerPipelineTest {

    @Test
    public void check_locales_choices() {
        final Script script = given_mocked_context("01-PHARMA-CreateIVTracerDeliveryPipeline");
        Assert.assertEquals(Arrays.asList("FR", "EN", "DE", "NL", "RO"),
                script.invokeMethod("localesChoices", null));
    }

    @Test
    public void check_database_types_choices() {
        final Script script = given_mocked_context("01-PHARMA-CreateIVTracerDeliveryPipeline");
        Assert.assertEquals(Arrays.asList("Timescale", "EnterpriseDB Advanced Server 11", "PostgreSQL",
                "MS SQLServer", "Oracle"), script.invokeMethod("databaseTypeChoices", null));
    }

    @Test
    public void check_UI_packagings_choices() {
        final Script script = given_mocked_context("01-PHARMA-CreateIVTracerDeliveryPipeline");
        Assert.assertEquals(Arrays.asList("DOCKER_DEBIAN", "DOCKER_REDHAT", "WINDOWS"),
                script.invokeMethod("uiPackagingChoices", null));
    }

    @Test
    public void check_versions_choices() {
        final Script script = given_mocked_context("01-PHARMA-CreateIVTracerDeliveryPipeline");
        Assert.assertEquals(Arrays.asList("2.3.1-SNAPSHOT", "2.3.0.0", "2.2.3", "2.2.1.3"),
                script.invokeMethod("versionChoices", null));
    }

    @Test
    public void check_parameters_when_all_fine() {
        final Script script = given_mocked_context("01-PHARMA-CreateIVTracerDeliveryPipeline");
        final Map<String, Object> params = new HashMap<String, Object>() {
            {
                put("company", "nc");
                put("town", "veynes");
                put("serviceAccountLogin", "admin");
                put("serviceAccountPassword", "admin");
                put("siteName", "Site de test");
                put("ivtracerCoreVersion", "2.3.0.0");
                put("locale", "EN");
                put("databaseType", "PostgreSQL");
                put("uiPackaging", "DOCKER_REDHAT");
            }
        };
        getBinding().setVariable("params", ImmutableMap.copyOf(params));
        script.invokeMethod("checkParametersOrDie", params);
    }

    @Test(expected = Exception.class)
    public void company_has_special_character() {
        final Script script = given_mocked_context("01-PHARMA-CreateIVTracerDeliveryPipeline");
        final Map<String, Object> params = new HashMap<String, Object>() {
            {
                put("company", "nc!!");
                put("town", "veynes");
                put("serviceAccountLogin", "admin");
                put("serviceAccountPassword", "admin");
                put("siteName", "Site de test");
            }
        };
        getBinding().setVariable("params", ImmutableMap.copyOf(params));
        script.invokeMethod("checkParametersOrDie", params);
    }

    @Test(expected = Exception.class)
    public void company_has_space() {
        final Script script = given_mocked_context("01-PHARMA-CreateIVTracerDeliveryPipeline");
        final Map<String, Object> params = new HashMap<String, Object>() {
            {
                put("company", "nc ffg");
                put("town", "veynes");
                put("serviceAccountLogin", "admin");
                put("serviceAccountPassword", "admin");
                put("siteName", "Site de test");
            }
        };
        getBinding().setVariable("params", ImmutableMap.copyOf(params));
        script.invokeMethod("checkParametersOrDie", params);
    }

    @Test(expected = Exception.class)
    public void town_has_space() {
        final Script script = given_mocked_context("01-PHARMA-CreateIVTracerDeliveryPipeline");
        final Map<String, Object> params = new HashMap<String, Object>() {
            {
                put("company", "nc");
                put("town", "veynes haute");
                put("serviceAccountLogin", "admin");
                put("serviceAccountPassword", "admin");
                put("siteName", "Site de test");
            }
        };
        getBinding().setVariable("params", ImmutableMap.copyOf(params));
        script.invokeMethod("checkParametersOrDie", params);
    }

    @Test(expected = Exception.class)
    public void town_has_special_character() {
        final Script script = given_mocked_context("01-PHARMA-CreateIVTracerDeliveryPipeline");
        final Map<String, Object> params = new HashMap<String, Object>() {
            {
                put("company", "nc");
                put("town", "marcy l'étoile");
                put("serviceAccountLogin", "admin");
                put("serviceAccountPassword", "admin");
                put("siteName", "Site de test");
            }
        };
        getBinding().setVariable("params", ImmutableMap.copyOf(params));
        script.invokeMethod("checkParametersOrDie", params);
    }

    @Override
    Script given_mocked_context(final String scriptPath) {
        return super.given_mocked_context(scriptPath);
    }
}
