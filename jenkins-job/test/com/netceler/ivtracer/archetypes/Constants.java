package com.netceler.ivtracer.archetypes;

public class Constants {

    enum CoreVersion {
        V_2_3_1_SNAPSHOT("2.3.1-SNAPSHOT", "2.3.1-SNAPSHOT", "2.3-0.1-SNAPSHOT", "2.3.1-SNAPSHOT",
                "1.4.0-SNAPSHOT"), V_2_3_0_0("2.3.0.0", "2.3.0.0", "2.3-0.0", "2.3.0.0", "1.3.4"), V_2_2_3(
                        "2.2.3", "2.2.3", "2.2-3.0", "2.2.3",
                        "1.3.3"), V_2_2_1_3("2.2.1.3", "2.2.1.2.2", "2.2-1.1.3", "2.2.1.2.1", "1.3.2");

        public final String coreVersion;

        public final String databaseVersion;

        public final String uiVersion;

        public final String cockpitVersion;

        public final String flyboxVersion;

        CoreVersion(final String coreVersion, final String databaseVersion, final String uiVersion,
                final String cockpitVersion, final String flyboxVersion) {
            this.coreVersion = coreVersion;
            this.databaseVersion = databaseVersion;
            this.uiVersion = uiVersion;
            this.cockpitVersion = cockpitVersion;
            this.flyboxVersion = flyboxVersion;
        }

        @Override
        public String toString() {
            return this.coreVersion;
        }
    }

    enum UIPackaging {
        DOCKER_DEBIAN("mongodb", "build,ssl,deb,mongodb,monitoring,offline"), DOCKER_REDHAT("mongodb",
                "build,ssl,rpm,mongodb,monitoring,offline"), WINDOWS("jpa",
                        "build,ssl,nomad,embeddedpgsql,windows,offline");

        public final String chartDBType;

        public final String buildProfiles;

        UIPackaging(final String chartDBType, final String buildProfiles) {
            this.chartDBType = chartDBType;
            this.buildProfiles = buildProfiles;
        }
    }

    enum DatabaseType {
        TIMESCALE_12("Timescale", "timescale12", "postgresql"), EDB_AS11("EnterpriseDB Advanced Server 11",
                "edb-as11", "postgresql"), POSTGRESQL("PostgreSQL", "postgresql", "postgresql"), SQLSERVER(
                        "MS SQLServer", "sqlserver", "sqlserver"), ORACLE("Oracle", "oracle", "oracle");

        public final String label;

        public final String type;

        public final String profile;

        DatabaseType(final String label, final String type, final String profile) {
            this.label = label;
            this.type = type;
            this.profile = profile;
        }

        @Override
        public String toString() {
            return this.label;
        }
    }

    enum Locale {
        FR("fr", "FR", "Europe/Paris", "fr"), EN("en", "EN", "Europe/Paris", "en"), DE("de", "DE",
                "Europe/Paris",
                "en"), NL("nl", "BE", "Europe/Brussels", "en"), RO("ro", "RO", "Europe/Bucharest", "en");

        public final String lang;

        public final String country;

        public final String timezone;

        public final String buildLang;

        Locale(final String lang, final String country, final String timezone, final String buildLang) {
            this.lang = lang;
            this.country = country;
            this.timezone = timezone;
            this.buildLang = buildLang;
        }
    }
}
