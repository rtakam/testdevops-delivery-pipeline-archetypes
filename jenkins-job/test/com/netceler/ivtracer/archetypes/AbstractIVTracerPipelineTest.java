package com.netceler.ivtracer.archetypes;

import static com.google.common.collect.ImmutableMap.of;
import static com.lesfurets.jenkins.unit.MethodSignature.method;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;

import com.lesfurets.jenkins.unit.BasePipelineTest;
import com.lesfurets.jenkins.unit.MethodCall;

import java.util.HashMap;
import java.util.List;

import groovy.lang.Closure;
import groovy.lang.Script;

public abstract class AbstractIVTracerPipelineTest extends BasePipelineTest {

    @Override
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    void assertThatCallExist(final MethodCall call, final String methodName, final String argsAsString) {
        assertThat(call.getMethodName()).isEqualTo(methodName);
        assertThat(call.argsToString()).contains(argsAsString);
    }

    void assertThatCallNotExist(final MethodCall call, final String methodName, final String argsAsString) {
        assertThat(call.getMethodName()).isNotEqualTo(methodName);
    }

    void assertThatCallNotExistWithArguments(final MethodCall call, final String argsAsString) {
        assertThat(call.argsToString()).doesNotContain(argsAsString);
    }

    Script given_mocked_context(final String scriptPath) {
        final Script script = loadScript(scriptPath);

        getHelper().registerAllowedMethod(method("checkout", Object.class), o -> {
        });
        getHelper().registerAllowedMethod(method("tool", Object.class), o -> {
        });
        getHelper().registerAllowedMethod(method("withEnv", List.class, Closure.class), (Closure) null);
        getHelper().registerAllowedMethod(method("booleanParam", HashMap.class),
                new BooleanParamClosure(this));
        getHelper().registerAllowedMethod(method("choice", HashMap.class), (Closure) null);
        getHelper().registerAllowedMethod(method("parameters", Object.class), (Closure) null);
        getHelper().registerAllowedMethod(method("wrap", HashMap.class, Closure.class), (Closure) null);

        getBinding().setVariable("params",
                of("databaseType", "oracle", "encryptPassPhrase", "marcy", "customerName", "marcy"));

        getBinding().setVariable("env", of("JOB_NAME", "ivtracer-dev/master"));
        getBinding().setVariable("tools",
                of("NODEJS_10", "NodeJS10", "ANGULARJS_NODE_LABEL", "angularjs", "JAVA_NODE_LABEL", "java"));
        getBinding().setVariable("scm", new Object());

        return script;
    }

    static class BooleanParamClosure extends Closure {

        public BooleanParamClosure(final Object owner) {
            super(owner);
        }

        public Object doCall(final HashMap args) {
            return args.get("name");
        }
    }
}
